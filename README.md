
INTRODUCTION
------------

This module is created  to so that users can delete their profiles.

 * The 'Delete Profile' button is available to the users.
 
 * When they wish to update profile through 'Edit profile Page'.
  
 * Use of this button by users will delete the profile as well as.
 
 * The content related to the user. That was the summary for 7.x-1.1.

 
REQUIREMENTS
------------

This module requires the following modules:

 * userprofiledelete (https://www.drupal.org/project/userprofiledelete)


INSTALLATION
------------

Install as usual, see
https://drupal.org/documentation/install/modules-themes/modules-7 for further
information.


CONFIGURATION
---------------

The permission of which type of user are allowed to delete their profiles should
be given by the admin through Permission page.

 * Use of this button by users will delete the profile as well as.
 
 * The content related to the user. That was the summary for 7.x-1.1


MAINTAINERS
-----------

Current maintainers:

 * Shashank (shashank5563) - https://www.drupal.org/u/shashank5563
